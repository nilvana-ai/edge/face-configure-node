const axios = require('axios')

module.exports = function (RED) {
  "use strict";
  function ConfigNode(n) {
    RED.nodes.createNode(this, n);
    this.topic = n.topic;
    this.interval = n.interval;
    this.camId = n.camId
    this.active = n.active
    this.qos = parseInt(n.qos);
    if (isNaN(this.qos) || this.qos < 0 || this.qos > 2) {
      this.qos = 1;
    }
    this.broker = n.broker;
    this.brokerConn = RED.nodes.getNode(this.broker);

    if (!/^(#$|(\+|[^+#]*)(\/(\+|[^+#]*))*(\/(\+|#|[^+#]*))?$)/.test(this.topic)) {
      return this.warn(RED._("mqtt.errors.invalid-topic"));
    }
    this.datatype = n.datatype || "utf8";

    var node = this;
    node.intervalTimer = false

    function verboseWarn (logMessage) {
      if (RED.settings.verbose) {
        node.warn(' -> ' + logMessage + node.serverInfo)
      }
    }
    function isCameraActive(cameras, camera_id){
      if ( cameras && cameras.length ){
        for (var i = 0; i < cameras.length; i++){
          if (cameras[i].id == camera_id){
            if (cameras[i].active && cameras[i].rtsp_path != ""){              
              return true                
            }
            verboseWarn("camera is disable")
            return false                  
          }            
        }
      }
      return false
    }

    node.checkCamera = function() {
      let mediaServerPort=8888
      let mediaServerHost="nilvana-media-server"      
      if (node.active!=true){
        node.status({});
        return
      }
      axios.get(`http://${mediaServerHost}:${mediaServerPort}/cameras`)
      .then((resp) => {
        if (node.camId === undefined){          
          return
        }        
        const cameras = resp.data.cameras
        if ( isCameraActive(cameras, node.camId) ){
          node.status({
            fill: "green",
            shape: "dot",
            text: "camera-configuration.enable-camera"
          });
          return
        }else{          
          node.status({
            fill: "red",
            shape: "dot",
            text: "camera-configuration.disable-camera"
          });
          return
        }        
      })
      .catch((error) => {
        console.error(error)        
        return;
      })
    }
    node.resetIntervalToCheckCamera = function (node) {
      if (node.intervalTimer) {
        verboseWarn('resetIntervalToCheckCamera node ' + node.id)
        clearInterval(node.intervalTimer)
      }
      node.intervalTimer = null
    }    

    node.startIntervalTimer = function () {
      if (!node.intervalTimer) {
        verboseWarn('startIntervalTimer node ' + node.id)
        node.intervalTimer = setInterval(node.checkCamera, 3000)
      }
    }

    if (this.brokerConn) {
      this.status({
        fill: "red",
        shape: "ring",
        text: "node-red:common.status.disconnected"
      });
      if (this.topic) {
        if (this.brokerConn.connected) {
          node.status({
            fill: "green",
            shape: "dot",
            text: "node-red:common.status.connected"
          });
        }
      } else {
        this.error(RED._("mqtt.errors.not-defined"));
      }
      this.on('close', function (removed, done) {
        if (node.brokerConn) {
          node.brokerConn.deregister(node, done);
        }
        node.resetIntervalToCheckCamera(node)
      });
      node.startIntervalTimer()

    } else {
      this.error(RED._("mqtt.errors.missing-config"));
    }
  }

  RED.nodes.registerType("camera-configuration", ConfigNode);

  RED.httpAdmin.post("/camera-configuration/:id/:state", RED.auth.needsPermission("camera-configuration.write"), function (req, res) {
    var state = req.params.state;
    var node = RED.nodes.getNode(req.params.id);

    if (node === null || typeof node === "undefined") {
      res.sendStatus(404);
      return;
    }
    var payload = req.body.msg
    if (state === "enable") {
      let enableMsg = {
        topic: node.topic,
        payload: payload,
        qos: Number(node.qos || 1),
        retain: node.retain || false
      };

      node.brokerConn.publish(enableMsg)
      node.active = true;

      res.send('activated');
      node.status({
        fill: "yellow",
        shape: "dot",        
        text: "camera-configuration.start-streaming"
      });
    } else if (state === "disable") {
      let disableMsg = {
        topic: node.topic,
        payload: payload,
        qos: Number(node.qos || 0),
        retain: node.retain || false
      };
      node.brokerConn.publish(disableMsg)
      node.active = false;
      res.send('deactivated');
      node.status({
        fill: "yellow",
        shape: "dot",        
        text: "camera-configuration.stop-streaming"
      });
    } else {
      res.sendStatus(404);
    }
  });  
  const mediaServerHost = "nilvana-media-server";
  const mediaServerPort = "8888";

  RED.httpAdmin.get("/get-cameras/:id", RED.auth.needsPermission("camera-configuration.write"), function (req, res) {
    var node = RED.nodes.getNode(req.params.id);

    axios.get(`http://${mediaServerHost}:${mediaServerPort}/cameras`)
      .then((resp) => {
        res.send(resp.data);
        return
      })
      .catch((error) => {
        console.error(error)
        res.sendStatus(404);
        return;
      })
  });

  RED.httpAdmin.get("/set-cameras/:id/:cam_id", RED.auth.needsPermission("camera-configuration.write"), function (req, res) {
    var node = RED.nodes.getNode(req.params.id);

    axios.post(`http://${mediaServerHost}:${mediaServerPort}/cameras/${req.params.cam_id}`, { enable: true, stream_type: "rtsp" })
      .then((resp) => {
        res.send(resp.data);
        return
      })
      .catch((error) => {
        console.error(error)
        res.sendStatus(404);
        return;
      })
  });
};
